
{-# LANGUAGE OverloadedStrings #-}

import Data.Text.Lazy
import Web.Scotty
import Network.HTTP.Types
import Environment

w :: World
w = ""

strToCID :: String -> ClientID
strToCID s = read s :: ClientID

main = scotty 3000 $ do
  get "/:client_id" $ do
    cids <- param "client_id"
    let cid = strToCID cids
    let view = worldView w cid
    html $ mconcat ["<h1>", fromStrict view, "</h1>"]
  post "/:client_id" $ do
    cid <- param "client_id"
    text $ mconcat ["Client ", cid, " made turn"]
