{-# LANGUAGE OverloadedStrings #-}

module Environment where
import Data.Text

type World = String

type Pace = Int
type Intention = String
--type Move = Intention Pace

--type Map [(Int,Int), Object] - расположение объектов
--type Apply_actions = Map [Action] Map
--Action РАЗБЕРЕМСЯ
--type Object = Collisions Bool

--stone :: Object
--apple :: Object

--План создания простой интерактивной системы взаимодействия агентов (микробов):

--модуль среды
--модуль агентуры
--модуль коммуникации между агентами

--Агенты могут взаимодействовать НЕ ЗНАЯ ДРУГ ДРУГА опосредованно, 
--сталкиваясь с эффектами действий неизвестных агентов как готовой ситуацией
--"НАГРАДА - это сигнал о том, что нам нужно сделать, чтобы угодить людям
--о которых мы ничего не знаем"

type ClientID = Integer

worldView :: World -> ClientID -> Text
worldView w cid =
  "000\n\
  \0@0\n\
  \000\n"
